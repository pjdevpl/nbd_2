var result = db.people.aggregate([
	{$project: {nationality: '$nationality', bmi: { $divide: [ '$weight', { $pow: [{$divide: ['$height', 100]}, 2] }] }}},
	{$group: {_id: '$nationality', avg_bmi: {$avg: '$bmi'}, min_bmi: {$min: '$bmi'}, max_bmi: {$max: '$bmi'}}}
]);
printjson(result._batch);
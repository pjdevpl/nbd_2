var result = db.people.aggregate([
	{$match: {sex: 'Female', 'nationality': 'Poland'}},
	{$unwind: '$credit'},
	{$group: {_id: '$credit.currency', sum_balance: {$sum: '$credit.balance'}, avg_balance: {$avg: '$credit.balance'}}}
]);
printjson(result._batch);
﻿db.people.mapReduce(
    function (){
        emit(this.sex, {sum_weight: this.weight, sum_height: this.height, count: 1});
    },
    function (key, values) {
        var weights = values.map(function(o) { return o.sum_weight});
        var heights = values.map(function(o) { return o.sum_height});
        var counts = values.map(function(o) { return o.count});
        return { sum_weight: Array.sum(weights), sum_height: Array.sum(heights), count: Array.sum(counts)};
    },
    {
        out: {inline: 1},
        finalize: function(key, reducedValue) {
            return {avg_weight: reducedValue.sum_weight/reducedValue.count, avg_height: reducedValue.sum_height/reducedValue.count};
        }
    }
);
db.people.mapReduce(
    function (){
        this.credit.forEach(function (c) {
            emit(c.currency, c.balance)
        });
    },
    function (key, values) {
        return Array.sum(values);
    },
    {
        out: {inline: 1}
    }
);
db.people.mapReduce(
    function (){
        var bmi = this.weight / Math.pow(this.height / 100, 2);
        emit(this.nationality, {bmi: bmi, sum_bmi: bmi, count: 1, min_bmi: bmi, max_bmi: bmi});
    },
    function(key, values) {
        var sum_bmis = values.map(function(b) {return b.sum_bmi});
        var counts = values.map(function(b) {return b.count});
        var min_bmis = values.map(function(b) {return b.min_bmi});
        var max_bmis = values.map(function(b) {return b.max_bmi});
        return {sum_bmi: Array.sum(sum_bmis), count: Array.sum(counts), min_bmi: Math.min.apply(Math, min_bmis), max_bmi: Math.max.apply(Math, max_bmis)};
    },
    {
        out: {inline: 1},
        finalize: function(key, reducedValue) {
            return {avg_bmi: reducedValue.sum_bmi/reducedValue.count, min_bmi: reducedValue.min_bmi, max_bmi: reducedValue.max_bmi};
        }
    }
);
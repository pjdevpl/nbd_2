db.people.mapReduce(
    function (){
        this.credit.forEach(function (c) {
            emit(c.currency, {sum: c.balance, avg: c.balance})
        });
    },
    function(key, values) {
        var sums = values.map(function(b) {return b.sum});
        return {sum: Array.sum(sums), avg: Array.avg(sums)};
    },
    {
        out: {inline: 1},
        query: {sex: 'Female', nationality: 'Poland'}
    }
);
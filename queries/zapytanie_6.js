db.people.insert({ 
    "sex" : "Male", 
    "first_name" : "Piotr", 
    "last_name" : "Jedruszuk", 
    "job" : "Software Engineer", 
    "email" : "s10888@pjatk.edu.pl", 
    "location" : {
        "city" : "Warsaw", 
        "address" : {
            "streetname" : "Spokojna", 
            "streetnumber" : "25"
        }
    }, 
    "description" : "To ja!", 
    "height" : 170.10, 
    "weight" : 71.22, 
    "birth_date" : "1988-06-15T16:00:05Z", 
    "nationality" : "Poland", 
    "credit" : [
        {
            "type" : "visa", 
            "number" : "3549325274085474", 
            "currency" : "PLN", 
            "balance" : "2510.68"
        }
    ]
});